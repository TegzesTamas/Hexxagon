#include "game.h"
#include "memtrace.h"

Player::Player(Color color,Game& game):game(game),color(color){}

bool Player::canStep(){
    for(int i = -TABLEOFFSET; i<=TABLEOFFSET; ++i){
        for(int j = -TABLEOFFSET; j<=TABLEOFFSET; ++j){
            //std::cout << "LOOKING FOR THE RIGHT COLOR " << i << " " << j << std::endl;
            if(game[i][j] == color){
                for(int di = std::max(i-2,-TABLEOFFSET); di <=std::min(i+2,TABLEOFFSET); ++di){
                    for(int dj = std::max(j-2,-TABLEOFFSET); dj <=std::min(j+2,TABLEOFFSET); ++dj){
                        //std::cout << "STEP CHECK " << i << " " << j << " " << di << " " << dj << std::endl;
                        if(game[di][dj] == BLANK && dist(i,j,di,dj) <= 2)
                            return true;
                    }
                }
            }

        }
    }
    return false;
}
