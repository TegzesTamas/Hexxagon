#include "tile.h"
#include "memtrace.h"

void Tile::convert(Color opponent){
    if(color != BLANK && color != INVALID)
        color = opponent;
}

void Tile::spread(Color movingPlayer){
    if(color == BLANK)
        color = movingPlayer;
}

char Tile::toChar() const{
    if(color == INVALID)
        return '*';
    if(color == BLANK)
        return '0';
    if(color == RED)
        return 'R';
    if(color == BLUE){
        return 'B';
    }
    return ' ';
}

std::ostream& operator<<(std::ostream& os, const Tile& tile){
    os << tile.toChar();
    return os;
}


