#ifndef GAME_H
#define GAME_H

#include <stdexcept>
#include <iostream>

#include "game.h"
#include "position.h"
#include "misc.h"
#include "tablecol.h"
#include "tile.h"
#include "step.h"
#include "misc.h"
#include "memtrace.h"

class Game;
/**
 *Abstract base class providing an interface to all kinds of players.
 */
class Player{
protected:
    ///The game this player is playing
    Game& game;
    ///The color of this player's pieces
    Color color;
public:

    /**
     *Creates a player with the given color playing the given game.
     *@param color The color of the player's pieces on the table
     *@param game The game this player is playing.
     */
    Player(Color color,Game& game);

    /**
     *Getter method for the color of the player.
     *@return The color of this player's pieces.
     */
    Color getColor(){return color;}

    /**
     *Tells the player that they should make a step.
     *@return Step The step this player would like to make. It is not guaranteed to be valid.
     */
    virtual Step yourTurn()=0;

    /**
     *Checks if the Player has a legal Step available.
     *@return false if the Player can't step and the game is over, true otherwise.
     */
    bool canStep();

     /**
      *Destructor
      */
    virtual ~Player(){}
};


/**
 *Contains the information and methods needed to run the game and enforces the rules.
 */
class Game{
private:
    ///Outputstream where state of the game is written.
    std::ostream& os;
    ///The table
    TableCol cols[TABLECOLSIZE];
    ///Players playing this game. Elements are not deleted in the destructor.
    Player* players[PLAYERNUM];
    ///Number of players playing this game.
    int playerNum;
    ///Index of the player whose turn it is at the moment.
    int playerOnTurn;

    /**
     *Used to access the table internally.
     *Only used internally.
     *@throw std::out_of_range {Index not in range}
     *@param i Index in the hexagonal coordinate system. Shows which column the tile is in.
     *@param j Index in the hexagonal coordinate system. Shows which tilted row the tile is in.
     *@return reference to the tile, can be modified.
     */
    Tile& tileAt(int i,int j){
        if(i+TABLEOFFSET < 0 || i+TABLEOFFSET > TABLECOLSIZE)
            throw std::out_of_range("Invalid index in setTile");
        return cols[i+TABLEOFFSET][j];
    }

    /**
     *Used to access the table internally.
     *Only used internally.
     *@param p Position of the tile in the hexagonal coordinate system.
     *@return reference to the tile, can be modified.
     */
    Tile& tileAt(Position p){
        return tileAt(p.x,p.y);
    }


    /**
     *Sets the color of a tile.
     *Only used internally.
     *Coordinates given in hexagonal format.
     *@param i The column the Tile to set is in.
     *@param j The tilted row the Tile to set is in.
     *@param color The new color of the tile.
     */
    void setTile(int i, int j, Color color){
        tileAt(i,j) = color;
    }

    /**
     *Sets the color of a tile.
     *Only used internally.
     *Position given in hexagonal format.
     *@param p Position of the tile in the hexagonal coordinate system.
     *@param color The new color of the tile.
     */
    void setTile(Position p,Color color){
        tileAt(p) = color;
    }


    /**
     *Starts the game.
     *It calls the Player::yourTurn() method for each player sequentially until one player has no possibility of making a valid step.
     *It checks if the steps returned by the players fit the rules, and executes them if they are.
     *When the player on turn has no possibility of making a valid step, the rest of the blank tiles are assumed as the
     */
    void run();


    /**
     *Checks if the step in the parameter is a valid step for the player on turn, and executes it if it is.
     *@param step Step to execute
     *@throw std::logic_error {Step is against the rules}
     */
    void executeStep(const Step& step);

    /**
     *Converts every Tile adjacent to the Position given, that belongs to an other player.
     *Called to convert the adjacent Tiles to the destination of a Step.
     *@param p Tiles adjacent to p will be converted.
     *@param color Color to convert to.
     */
    void convertAdjacent(Position p,Color color);

public:
    /**
     *Creates a game.
     *Initializes the Tiles on the board according to rules.
     */
    Game(std::ostream& os);

    /**
     *Adds a player to the game.
     *The player's Player::yourTurn() method will be called if the player is on turn.
     *If the desired amount of players is reached (defined in the PLAYERNUM macro) run() is called.
     *@param newPlayer pointer to the new Player. Will not be deallocated by the object.
     */
    void attachPlayer(Player* newPlayer);

    /**
     *Converts the hexagonal index to array index and returns a constant reference to the TableCol at that index.
     *TableCol can be indexed with hexagonal index to get constant reference to the Tile at the desired position.
     *@param i Index of the TableCol to be returned
     *&return const reference to the TableCol at that position. Can be indexed to get a Tile.
     */
    const TableCol& operator[](int i)const{
        if(i+TABLEOFFSET < 0 || i+TABLEOFFSET > TABLECOLSIZE)
            throw std::out_of_range("Invalid indexing of Game");
        return cols[i+TABLEOFFSET];
    }

    /**
     *Converts the hexagonal position to array position and returns a constant reference to the Tile at the correct place.
     *@param p Hexagonal position of the tile to be reached.
     *&return const reference to the Tile at that position.
     */
    const Tile& operator[](Position p)const{
        return (*this)[p.x][p.y];
    }
    /**
     *Prints the current state of the table to the Game's outputstream.
     */
    void printTable();
};

#endif // GAME_H
