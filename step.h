#ifndef STEP_H
#define STEP_H

#include "position.h"

#include <istream>
#include "memtrace.h"


/**
 *Describes the step of a player in the game.
 *It is used by the Players to interact with the Game in a supervisable way: the Player returns a Step, then the Game checks if the Step is legal, then executes it.
 */
struct Step{
    ///The Position of the origin of the Step
    Position origin;
    ///The Position of the goal of the Step
    Position goal;
    ///Constructs an invalid step with both positions being the center of the table.
    Step():origin(0,0),goal(0,0){}
    /**
     *Constructs a step with the two given positions.
     *@param origin The position from which the Player wants to step
     *@param goal The position where the Player wants to step
     */
    Step(Position origin, Position goal):origin(origin),goal(goal){}

    /**
     *Checks if the step can be a legal move.
     *If this returns false, the step is definitely illegal, however if it returns true, it might still be illegal,
     *if the origin does not belong to the Player stepping or the goal is not BLANK.
     *@return false if the step cannot be legal, true if it might.
     */
    bool valid() const{
        return origin.valid() && goal.valid() && origin != goal && origin.dist(goal) <= 2;
    }
};
/**
 *Reads a step from the given input stream.
 *The format should be "<origin x> <origin y> <goal x> <goal y>", all in hexagonal coordinates.
 *@param is The input stream to read the step from.
 *@param step the Step that the data will be read to
 */
std::istream& operator>>(std::istream& is, Step& step);

#endif // STEP_H
