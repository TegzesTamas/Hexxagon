#ifndef AIPLAYER_H
#define AIPLAYER_H


#include <iostream>

#include "game.h"
#include "misc.h"
#include "position.h"
#include "memtrace.h"

/**
 *Makes decisions to step on it's own. Can be interfaced the same as any Player.
 */
class AIPlayer : public Player
{
private:
    /**
     *Determines if the player can step to the Tile at the position given.
     *@param i Coloumn of the tile to be checked
     *@param j Row of the tile to be checked
     *@param from After the function returns Position of one of the closest Tiles belonging to this Player
     *@return Distance of the closest Tile to the destination belonging to this Player
     */
    int closestOwnTile(int i, int j, Position& from);
    /**
     *Counts the number of Tiles that will change to the players Color, if the player steps at the given Position and decides where is it best to step there from.
     *@param i The column of the goal of the step
     *@param j The row of the goal of the step
     *@return The number of Tiles that would be gained if this step would be executed
     */
    int evaluateDestination(int i, int j, Position& from);
public:
    AIPlayer(Color color,Game& game):Player(color,game){}
    virtual Step yourTurn();
    virtual ~AIPlayer(){}
};

#endif // AIPLAYER_H
