#include "step.h"
#include "memtrace.h"

std::istream& operator>>(std::istream& is, Step& step){
    int x1,y1,x2,y2;
    is >> x1 >> y1 >> x2 >> y2;
    step.origin = Position(x1,y1);
    step.goal = Position(x2,y2);
    return is;
}
