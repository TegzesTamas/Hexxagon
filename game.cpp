#include "game.h"

#include "memtrace.h"


Game::Game(std::ostream& os):os(os),playerNum(0),playerOnTurn(0){
        Position origo(0,0);
        for(int i = -TABLEOFFSET; i <= TABLEOFFSET; ++i){
            for(int j = -TABLEOFFSET; j <= TABLEOFFSET; ++j){
                if(origo.dist(i,j) > TABLEOFFSET){
                    setTile(i,j,INVALID);
                }
            }
        }
        setTile(0,-1,INVALID);
        setTile(-1,1,INVALID);
        setTile( 1,0,INVALID);
}

void Game::attachPlayer(Player* newPlayer){
        if(playerNum >= PLAYERNUM) throw std::out_of_range("Too many players");
        players[playerNum++] = newPlayer;
        if(playerNum == PLAYERNUM){
            setTile(0,-TABLEOFFSET,players[0]->getColor());
            setTile(-TABLEOFFSET,TABLEOFFSET,players[0]->getColor());
            setTile(TABLEOFFSET,0,players[0]->getColor());

            setTile(TABLEOFFSET,-TABLEOFFSET,players[1]->getColor());
            setTile(-TABLEOFFSET,0,players[1]->getColor());
            setTile(0,TABLEOFFSET,players[1]->getColor());
            run();
        }
    }

void Game::run(){
    printTable();
    while(players[playerOnTurn]->canStep()){
        os << players[playerOnTurn]->getColor()<< " moves" << std::endl;
        Step step = players[playerOnTurn]->yourTurn();
        try{
            executeStep(step);
            playerOnTurn = (playerOnTurn+1)%playerNum;
        }
        catch(std::logic_error e){
            os << e.what() << std::endl;
        }
        catch(std::ios_base::failure e){
            os << e.what() << std::endl;
        }
        printTable();
    }
    os << players[playerOnTurn]->getColor() << " can't move anymore..." << std::endl;
    int tilesOfPlayers[playerNum];
    for(int i = 0; i<playerNum; ++i){
        tilesOfPlayers[i] = 0;
    }
    int winner = 0;
    for(int i = -TABLEOFFSET; i <= TABLEOFFSET; ++i){
        for(int j = -TABLEOFFSET; j <= TABLEOFFSET; ++j){
            tileAt(i,j).spread(players[(playerOnTurn+1)%playerNum]->getColor());
            for(int k = 0; k<playerNum; ++k){
                if(tileAt(i,j) == players[k]->getColor()) ++tilesOfPlayers[k];
                if(tilesOfPlayers[k] > tilesOfPlayers[winner]) winner = k;
            }
        }
    }
    os << players[winner]->getColor() << " won!" << std::endl;

}

void Game::convertAdjacent(Position p, Color color){
    for(int i = -1; i<=1; ++i){
        for(int j = -1; j<=1; ++j){
            if(-2 < i+j && i+j < 2 && p.x +i <= TABLEOFFSET && p.x +i >= -TABLEOFFSET && p.y +j <= TABLEOFFSET && p.y +j >= -TABLEOFFSET){
                tileAt(p.x+i,p.y+j).convert(color);
            }
        }
    }
}

void Game::executeStep(const Step& step){
    os << step.origin.x << " " <<step.origin.y << " " << step.goal.x << " " << step.goal.y << std::endl;
    if(step.valid() &&
        operator[](step.origin).getColor() == players[playerOnTurn]->getColor() &&
        operator[](step.goal).getColor() == BLANK)
    {
        if(step.origin.dist(step.goal) == 2){
            setTile(step.origin,BLANK);
        }
        setTile(step.goal,players[playerOnTurn]->getColor());
        convertAdjacent(step.goal,players[playerOnTurn]->getColor());
    }
    else
        throw std::logic_error(std::string("Invalid step! ")
                               + std::string(step.valid()? "Step is valid" : "Step is invalid")
                               + std::string("; ") + std::string(operator[](step.origin).getColor() == players[playerOnTurn]->getColor()? "Origin color is right" : "Origin color is wrong")
                               + std::string("; ") + std::string(operator[](step.goal).getColor() == BLANK? "Destination is blank" : "Destination is not blank"));

}

void Game::printTable(){
    for(int i = -TABLEOFFSET; i<=TABLEOFFSET; ++i){
        for(int j = -TABLEOFFSET; j<=TABLEOFFSET; ++j){
            os << (*this)[j][i];
            std::cout << (*this)[j][i];
        }
        os << ' ' << std::endl;
    }
}
