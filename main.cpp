#include <iostream>
#include <fstream>
#include <stdexcept>

#include "misc.h"
#include "game.h"
#include "tile.h"
#include "testplayer.h"
#include "aiplayer.h"
#include "memtrace.h"

using std::cin;
using std::cout;

int main(){
    std::ifstream test;
    int testNumber;
    std::cin >> testNumber;
    try{
        if(testNumber == 1){
            Game game(cout);
            test.open("test1.txt");
            test.ignore(1);
            TestPlayer player1(BLUE,game,test);
            TestPlayer player2(RED,game,test);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);
        }
        else if(testNumber == 2){
            Game game(cout);
            test.open("test2.txt");
            test.ignore(1);
            TestPlayer player1(RED,game,test);
            TestPlayer player2(BLUE,game,test);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);
        }
        else if(testNumber == 3){
            Game game(cout);
            test.open("test3.txt");
            test.ignore(1);
            TestPlayer player1(RED,game,test);
            TestPlayer player2(BLUE,game,test);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);
        }
        else if(testNumber == 4){
            Game game(cout);
            test.open("test4.txt");
            test.ignore(1);
            TestPlayer player1(RED,game,test);
            TestPlayer player2(BLUE,game,test);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);

        }
        else if(testNumber == 5){
            Game game(cout);
            test.open("test5.txt");
            test.ignore(1);
            TestPlayer player1(RED,game,test);
            AIPlayer player2(BLUE,game);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);
        }
        else{
            Game game(cout);
            TestPlayer player1(BLUE,game,cin);
            AIPlayer player2(RED,game);
            game.attachPlayer(&player1);
            game.attachPlayer(&player2);
        }
        cout << "TEST SUCCESSFUL";
    }
    catch(std::exception& e){
        cout << "************************************" << std::endl
            << "ERROR "<< e.what() << std::endl
            << "************************************" << std::endl;
    }
    return 0;
}
