#include "aiplayer.h"
#include "memtrace.h"

int AIPlayer::closestOwnTile(int i, int j, Position& from){
    int minDist = 3;
    if(game[i][j] != BLANK) return -1;
    int x, y;
    for(x = mymax(i-2,-TABLEOFFSET); x <= mymin(i+2,TABLEOFFSET); ++x)
        for(y = mymax(j-2,-TABLEOFFSET); y <= mymin(j+2,TABLEOFFSET); ++y)
            if(game[x][y] == color){
                int cDist = dist(x,y,i,j);
                if(cDist < minDist){
                        minDist = cDist;
                        from.x = x;
                        from.y = y;
                }
            }
    return minDist;
}

int AIPlayer::evaluateDestination(int i, int j, Position& from){
    int d = closestOwnTile(i,j,from);
    int ret;
    if(d > 2) return 0;
    if(d == 2) ret = 0;
    if(d == 1) ret = 1;
    if(d < 1) return 0;
    for(int di = mymax(i-1,-TABLEOFFSET); di <=mymin(i+1,TABLEOFFSET); ++di)
        for(int dj=mymax(j-1,-TABLEOFFSET); dj<=mymin(j+1,TABLEOFFSET); ++dj)
            if(dist(di,dj,i,j) == 1 && game[di][dj] != BLANK && game[di][dj] != INVALID && game[di][dj] != color)
                ++ret;
    return ret;
}

Step AIPlayer::yourTurn(){
    int maxi=-1, maxj=-1;
    int mostGained = 0;
    Position stepOrigins[TABLECOLSIZE][TABLECOLSIZE];
    for(int i = 0; i<TABLECOLSIZE; ++i){
        for(int j = 0; j<TABLECOLSIZE; ++j){
            int newGained = evaluateDestination(i-TABLEOFFSET,j-TABLEOFFSET,stepOrigins[i][j]);
            if(newGained > mostGained){
                maxi = i;
                maxj = j;
                mostGained = newGained;
            }
        }
    }
    if(maxi == -1 || maxj == -1)
        throw std::logic_error("AIPlayer couldn't find step.");
    return Step(stepOrigins[maxi][maxj],Position(maxi-TABLEOFFSET,maxj-TABLEOFFSET));
}
