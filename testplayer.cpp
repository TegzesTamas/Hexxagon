#include "testplayer.h"
#include "memtrace.h"

Step TestPlayer::yourTurn(){
    char c = ' ';
    for(int i = -TABLEOFFSET; i <=TABLEOFFSET; ++i){
        for(int j = -TABLEOFFSET; j<=TABLEOFFSET; ++j){
            is >> c;
            if(c != game[j][i].toChar()){
                throw std::logic_error("State of the game not as expected!");
            }
        }
    }
    Step ret;
    is >> ret;
    return ret;
}
