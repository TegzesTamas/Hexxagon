#ifndef TABLECOL_H
#define TABLECOL_H

#include <iostream>
#include <stdexcept>
#include "tile.h"
#include "memtrace.h"

/**
 *Stores a column of Tiles. Implements operator[] to translate hexagonal coordinates to array coordinates.
 */
class TableCol{
private:
    Tile tiles[TABLECOLSIZE];
public:
    /**
     *Converts hexagonal coordinates to array coordinates and returns with the Tile stored there.
     *@return const reference to the Tile stored at the place
     *@throw std::out_of_range {There isn't a Tile with the requested index}
     */
    const Tile& operator[](int i) const{
        if(i+TABLEOFFSET < 0 || i+TABLEOFFSET > TABLECOLSIZE)
            throw std::out_of_range("Invalid indexing of TableCol");
        return tiles[i+TABLEOFFSET];
    };
    /**
     *Converts hexagonal coordinates to array coordinates and returns with the Tile stored there.
     *@return reference to the Tile stored at the place
     *@throw std::out_of_range {There isn't a Tile with the requested index}
     */
    Tile& operator[](int i){
        if(i+TABLEOFFSET < 0 || i+TABLEOFFSET > TABLECOLSIZE)
            throw std::out_of_range("Invalid indexing of TableCol");
        return tiles[i+TABLEOFFSET];
    }
};

#endif // TABLECOL_H
