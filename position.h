#ifndef POSITION_H
#define POSITION_H

#include "misc.h"
#include "memtrace.h"


/**
 *Calculates the number of (adjacent) steps needed to get from one Position to another.
 *Same as Position::dist(), but can be used with ints instead of Positions.
 *@param x1 Coloumn of one Position (Hexagonal)
 *@param y1 Row of one Position (Hexagonal)
 *@param x2 Coloumn of the other Position (Hexagonal)
 *@param y2 Row of the other Position (Hexagonal)
 *@return number of adjacent steps needed
 */
inline int dist(int x1, int y1, int x2, int y2){
    return mymax(myabs(x1 - x2),//Required steps on the x axis
               myabs(y1 - y2),//Required steps on the y axis
               myabs(x1 + y1 - x2 - y2));//Required steps on the virtual third axis
}
/**
 *Used to store positions on the hexagonal grid. It should be used with hexagonal coordinates.
 */
struct Position
{
        ///Index of the column
        int x;
        ///Index of the row
        int y;
        /**
         *Constructs a Position
         *@param x Index of the column in hexagonal coordinates
         *@param y Index of the row in hexagonal coordinates
         */
        Position(int x = 0, int y = 0):x(x),y(y){}
        /**
         *Calculates the number of steps (to adjacent tiles) needed to get from this position to another.
         *@param other reference to the other position
         *@return number of adjacent steps needed
         */
        int dist(const Position& other) const{
            return ::dist(this->x, this->y, other.x, other.y);
        }
        /**
         *Calculates the number of steps (to adjacent tiles) needed to get from this position to another.
         *@param x Column of the other Tile (Hexagonal)
         *@param y Row of the other Tile (Hexagonal)
         *@return number of adjacent steps needed
         */
        int dist(int x, int y) const{
            return ::dist(this->x, this->y, x, y);
        }
        /**
         *Returns whether this position is on the table
         *@return true if position is on the table, false otherwise
         */
        bool valid() const;

        /**
         *Compares two Positions
         *@param rhs The other Position
         *@return true if the Positions point to the same place, false if they don't
         */
        bool operator==(const Position& rhs) const{
            return (x==rhs.x)&&(y==rhs.y);
        }

        /**
         *Compares two Positions
         *@param rhs The other Position
         *@return false if the Positions point to the same place, true if they don't
         */
        bool operator!=(const Position& rhs) const{
            return !((*this) == rhs);
        }
};

#endif // POSITION_H
