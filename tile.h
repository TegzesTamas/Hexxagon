#ifndef TILE_H
#define TILE_H

#include <iostream>
#include "misc.h"
#include "memtrace.h"

/**
 *Stores the state of a tile on the Table. Implements functions to interface with it.
 */
class Tile{
    Color color;
public:

    /**
     *Constucts a Tile with the given color.
     *@param color initial color of the Tile
     */
    Tile(Color color = BLANK):color(color){};

    /**
     *Getter method for the color of the Tile
     *@return Color of the Tile
     */
    Color getColor() const {return color;};
    /**
     *Changes the color of the Tile to the color given.
     *@param color the new color of the Tile
     *@return the new color of the Tile
     */
    Color operator=(Color color){this->color = color; return color;}

    /**
     *Compares the color of this Tile to the given color.
     *@param color the Color to compare to.
     *@return true if the Tile has the color given, false otherwise
     */
    bool operator==(Color color)const{return this->color == color;}
    /**
     *Compares the color of this Tile to the given color.
     *@param color the Color to compare to.
     *@return false if the Tile has the color given, true otherwise
     */
    bool operator!=(Color color)const{return !((*this) == color);}

    /**
     *If the color of the Tile is not BLANK and not INVALID, then it changes to the opponents color.
     *Called when an adjacent Tile was the goal of a Step.
     *@param opponenet new color of the Tile if it converts
     */
    void convert(Color opponent);

    /**
     *If the color of the Tile is BLANK, it changes to the given color.
     *Used at the end of the game when a Player has no legal moves available. All the BLANK tiles then go to the Player, who still can move.
     *@param movingPlayer the Color of the player who can still move.
     */
    void spread(Color movingPlayer);
    /**
     *Returns a char representing the color of the Tile.
     *@return 'B' for BLUE, 'R' for RED, '*' for INVALID, and '0' for BLANK.
     */
    char toChar() const;
};

/**
 *Prints a Tile to an std::ostream.
 *Prints one character depending on the Color of the Tile:
 *'B' for BLUE,
 *'R' for RED,
 *'*' for INVALID, and
 *'0' for BLANK.
 *@param os the output stream which the characters print to
 *@param tile the Tile which gets printed
 *@return The output stream to make it chainable.
 */
std::ostream& operator<<(std::ostream& os, const Tile& tile);


#endif // TILE_H
