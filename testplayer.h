#ifndef TESTPLAYER_H
#define TESTPLAYER_H


#include <iostream>
#include <iomanip>

#include "game.h"
#include "step.h"
#include "memtrace.h"



/**
 *Player used for easy testing, it reads Steps from the given inputstream when asked to step.
 */
class TestPlayer : public Player{
private:
    std::istream & is;
public:
    /**
     *Creates a TestPlayer with the given Color, playing the given Game reading input from the given stream.
     *@param color The color of the Player's tiles
     *@param game The Game the Player is supposed to play
     *@param is The stream from which the player will read it's inputs.
     */
    TestPlayer(Color color,Game& game,std::istream& is):Player(color,game),is(is){}

    /**
     *Asks the TestPlayer to make a step.
     *Reads the supposed state of the board and compares it with the actual state of the board. Throws an exception if they don't match.
     *Then it reads a Step from the given inputstream, and returns it.
     *@return the step read from the stream
     */
    virtual Step yourTurn();
    virtual ~TestPlayer(){}
};

#endif // TESTPLAYER_H
