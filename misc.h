#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include <iostream>
#include "memtrace.h"

///The possible states of a Tile
enum Color{RED, BLUE, BLANK,INVALID};
///Length of the side of the table
const int TABLELENGTH = 5;
///Length of the longest column
const int TABLECOLSIZE = 2*TABLELENGTH - 1;
///Hexagonal coordinates are this much smaller than array coordinates
const int TABLEOFFSET = TABLELENGTH - 1;
///Number of players
const int PLAYERNUM = 2;

inline std::ostream& operator<< (std::ostream& os, Color c){
    os << (c==RED ? "RED" : "BLUE");
    return os;
}

/**
 *@return the absolute value of a.
 */
inline int myabs(int a){
    if(a<0) return -a;
    return a;
}

/**
 *@return the biggest of the two numbers
 */
inline int mymax(int a, int b){
    if(a > b){
        return a;
    }
    return b;
}

/**
 *@return the biggest of the three numbers
 */
inline int mymax(int a, int b, int c){
    return mymax(a, mymax(b,c));
}

/**
 *@return the smallest of the two numbers
 */
inline int mymin(int a, int b){
    if(a < b){
        return a;
    }
    return b;
}

#endif // MISC_H_INCLUDED
